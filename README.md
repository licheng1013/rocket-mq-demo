[toc]
# 目录
## RocketMQ
- 2022/5/12

## 学习版本
- rocketmq-rocketmq-all-4.9.3.zip
- 本地解压运行

### 官网
- https://rocketmq.apache.org/docs/quick-start/

### spring-boot-start依赖
- https://github.com/apache/rocketmq-spring

### docker文档
- https://github.com/apache/rocketmq-docker
- docker用来测试即可，生产不建议使用
- 需要修改 broker的 broker.conf

## 测试
- 负载均衡无问题。

### 异步双写测试
- 可能丢失消息
- 性能高
- 未测试

### 同步双写测试
- 安全最高不会丢失消息
- 性能比异步双写略低
- 未测试

### 多Master模式
- 在磁盘配置为RAID10时非常可靠
- 同步刷盘一条不丢，异步可能会丢失

### 部署文档
- https://github.com/apache/rocketmq/blob/master/docs/cn/operation.md
- console控制台: https://hub.docker.com/r/styletang/rocketmq-console-ng


## 业务
### 幂等
- 通过查看数据插入或者订单状态来判断
- 订单状态，此时可能出现重复消费的情况，引入乐观锁来解决问题
- 数据插入，在数据库设置唯一主键，防止重复拆入。


