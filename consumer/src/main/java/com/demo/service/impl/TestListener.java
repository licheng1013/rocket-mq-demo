package com.demo.service.impl;

import com.demo.rocket.RocketConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * @author lc
 * @since 2022/6/13
 */

@Slf4j
@Service
@RocketMQMessageListener(topic = RocketConfig.TEST, consumerGroup = RocketConfig.TEST_GROUP)
public class TestListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        log.info("收到测试消息：{}",s);
    }
}
