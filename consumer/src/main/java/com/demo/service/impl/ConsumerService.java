package com.demo.service.impl;

import com.demo.rocket.RocketConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 *
 * @since 2022/5/12
 */
@Slf4j
@Service
// 监听队列，这两个参数都是必须填的，主题名(生产端的主题必须和消费者一直，与生产者组可以不一样)，分组名(启动多个相同服务就是负责均衡)
@RocketMQMessageListener(topic = RocketConfig.KEY, consumerGroup = RocketConfig.GROUP)
public class ConsumerService implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        log.info("接受消息: {}", message);
    }
}
