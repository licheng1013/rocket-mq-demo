package com.demo.rocket;

/**
 * @author lc
 * @since 2022/5/12
 */
public class RocketConfig {
    public static final String KEY = "KEY";
    public static final String TEST = "TEST";
    public static final String GROUP = "GROUP";
    public static final String TEST_GROUP = "TEST_GROUP";
}
