package com.demo.rocket;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.MessageHeaderAccessor;

/**
 * @author lc
 * @since 2022/6/13
 */
public class MsgBuilder {
    public static <T> Message<T> msgBuilder(T msg){
        return MessageBuilder.withPayload(msg).build();
    }

    public static <T> Message<T> msgBuilder(T msg,MessageHeaderAccessor header){
        return MessageBuilder.withPayload(msg).setHeaders(header).build();
    }
}
